<?php
include_once("coursefeed.php");

$id = $_POST['id'];
$password = $_POST['password'];

$coursefeed = new CourseFeed();

$_SESSION['user'] = $coursefeed->login($id, $password);

if(isset($_SESSION['user']['name']))
{
    header("Location: index.php");
}
else
{
	$_SESSION['user'] = NULL;
    echo
    "<script>
    alert('login fail.');
    history.back();
    </script>";
}

?>