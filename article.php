<?php

require_once("coursefeed.php");
require_once("template/article.php");
require_once("template/article_list.php");
require_once("template/calendar.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

function validMonth($month) {
    return 1 <= $month && $month <= 12;
}

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    startblock('content');

    if (http\has_parameter($_GET, "id")) {
        $article = $coursefeed->getArticle($_GET["id"]);
        if ($article["latest_revision"]) {
            template\article\renderEntry($article);
        } else {
            header('HTTP/1.1 404 Not Found');
            template\article\renderNotFound();
        }
    } else {
        $page = (int)(http\maybe_get_parameter($_GET, "page"));
        if ($page === 0) {
            $page = 1;
        }
        $pageCount = ceil($coursefeed->getArticleCount() / 20);
        if (!(1 <= $page && $page <= $pageCount)) {
            $page = 1;
        }

        $articles = $coursefeed->getArticleList(http\maybe_get_parameter($_GET, "course_id"),
                                                ($page - 1) * 20);

        template\article\renderListHeader();

        if (http\has_parameter($_GET, "month") && validMonth($_GET["month"])) {
            template\calendar\renderCalendarForm($articles, $_GET["month"]);
        } else {
            $course_id = http\maybe_get_parameter($_GET, "course_id");
            if ($course_id !== null) {
                if (is_string($course_id)) {
                    $course_id = array($course_id);
                } else {
                    $course_id = explode(",", $course_id);
                }

                $course = $coursefeed->getCourse($course_id);;
            } else {
                $course = null;
            }

            template\article\renderList($articles, $page, $pageCount, $course);
        }
    }
    endblock();
}
?>
