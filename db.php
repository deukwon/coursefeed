<?php

namespace db;
use PDO;

error_reporting(E_ALL);
session_start();
ini_set('display_errors', 1);

date_default_timezone_set('UTC');

function has_all_keys($array, $keys) {
    foreach ($keys as $key) {
        if (!array_key_exists($key, $array)) {
            return false;
        }
    }
    return true;
}

function get_config() {
    if (array_key_exists("CLEARDB_DATABASE_URL", $_ENV)) {
        $config = parse_url(getenv("CLEARDB_DATABASE_URL"));
        $config["db"] = substr($config["path"], 1);
        unset($config["path"]);
    } else {
        $config = json_decode(file_get_contents("credential.json"), true);
    }
    assert(has_all_keys($config, array("host", "user", "pass", "db")));

    $config["dsn"] = sprintf("mysql:host=%s;dbname=%s", $config["host"], $config["db"]);
    unset($config["host"]);
    unset($config["db"]);

    assert(has_all_keys($config, array("dsn", "user", "pass")));
    return $config;
}

function connect() {
    $config = get_config();

    $pdo = new PDO($config["dsn"], $config["user"], $config["pass"]);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->exec("SET AUTOCOMMIT = FALSE");
    return $pdo;
}

?>
