<?php

namespace template\course;

?>

<? function renderInsertCourseForm() { ?>
	<h2>Add New Course</h2>
    <form method="post" class="pure-form pure-form-stacked">
	    <div><label>Name<input name="name" type="text" /></label></div>
	    <div><label>Code<input name="code" type="text" /></label></div>
	    <div><label>Year<select name="year">
	    	<? for ($i=date("Y")-1; $i <= date("Y")+1; $i++) { ?>
	    		<option value="<?=$i?>"><?=$i?></option>
	    	<? } ?>
	    </select></label></div>
	    <div><input class="pure-button pure-button-primary" type="submit" value="Add Course" /> | <a href="article.php">list</a></div>
	</form>
<? } ?>
