<?php

namespace template\loginform;

?>

<?php function renderLoginForm($name) {
    global $coursefeed;

    if($name == "") { ?>
    <form action="login.php" method="post" class="pure-form pure-form-stacked">
        <label>ID<input type="text" name="id" /></label>
        <label>Password<input type="password" name="password" /></label>
        <input type="submit" value="login" class="pure-button" /> or <a href="join.php">Join</a>.
    </form>
    <?php } else {
           ?>
        <p>안녕하세요. <?= $_SESSION['user']['name']?>님</p>
        <form action="logout.php" method="post" class="pure-form pure-form-stacked">
            <input type="submit" value="logout" class="pure-button" />
        </form>
        <ul>
        <? if ($coursefeed->isAdmin()) { ?>
            <li><a href="adminuser.php">Manage users</a></li>
            <li><a href="category_edit.php">Add New category</a></li>
        <? } ?>
        </ul>
    <?php } ?>
<?php } ?>
