<?php
require_once("coursefeed.php");
require_once("template/adminuser.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php');

if ($_SERVER['REQUEST_METHOD'] == "GET") {
	startblock('content');
    template\adminuser\renderTestPage($coursefeed->getUserList());
    endblock('content');
}
?>