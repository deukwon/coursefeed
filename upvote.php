<?php

require_once("coursefeed.php");
require_once("http.php");

$coursefeed = new CourseFeed();

ob_start();
header("Content-type: application/json");

function renderUpvoteCount($upvotes) {
    print json_encode(array("upvotes"=>$upvotes));
}

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (http\has_parameter($_GET, "article_id")) {
        renderUpvoteCount($coursefeed->getUpvoteCount($_GET["article_id"]));
    } else {
        header('HTTP/1.1 400 Bad Request');
    }
} else if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if ($coursefeed->isLoggedIn() && http\has_parameter($_POST, "article_id")) {
        $coursefeed->upvote($_POST["article_id"]);
        renderUpvoteCount($coursefeed->getUpvoteCount($_POST["article_id"]));
    } else {
        header('HTTP/1.1 400 Bad Request');
    }
} else {
    header('HTTP/1.1 405 Method Not Allowed');
}

ob_end_flush();
exit;

?>
